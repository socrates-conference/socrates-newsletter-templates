export type UUID = string
export type Subscriber = {
  name: string,
  email: string
}

export type Template = {
  title: string,
  subject: string,
  text: string,
  htmlText: string
}