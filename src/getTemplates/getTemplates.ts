import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {Template} from '../common/types'
import {success} from '../common/util'
import {
  ListTemplatesCommandOutput,
  SES,
  TemplateMetadata
} from '@aws-sdk/client-ses'

const getTemplate = async (ses, metadata: TemplateMetadata): Promise<Template> => {
  const {Template: template} = await ses.getTemplate({TemplateName: metadata.Name})
  return {
    title: template.TemplateName,
    subject: template.SubjectPart,
    htmlText: template.HtmlPart,
    text: template.TextPart
  }
}

// noinspection JSUnusedLocalSymbols
export const getTemplates = (ses: SES) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const result: ListTemplatesCommandOutput = await ses.listTemplates({MaxItems: 10})
  return result.TemplatesMetadata.length !== 0
         ? success(await Promise.all(result.TemplatesMetadata.map(r => getTemplate(ses, r))))
         : success([])
}