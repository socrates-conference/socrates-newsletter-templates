import {getTemplates} from './getTemplates'
import {SES} from '@aws-sdk/client-ses'

export const handler = getTemplates(new SES({region: 'eu-central-1'}))