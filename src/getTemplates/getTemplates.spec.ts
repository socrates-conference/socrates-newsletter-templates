import {
  FakeSES,
  SESTemplate
} from '../mock/ses'

import {APIGatewayProxyEvent} from 'aws-lambda'
import {getTemplates} from './getTemplates'
import {SES} from '@aws-sdk/client-ses'


describe('GET /templates', () => {
  let fakeSES: FakeSES
  beforeEach(() => {
    fakeSES = new FakeSES()
  })

  describe('without path parameters', () => {
    it('should return empty list', async () => {
      const result = await getTemplates(fakeSES as unknown as SES)({pathParameters: undefined} as APIGatewayProxyEvent)
      expect(result.statusCode).toEqual(200)
      expect(fakeSES.templateName).toBeUndefined()
      expect(JSON.parse(result.body)).toEqual([])
    })
    describe('when a template exists', () => {
      const templateName = 'something'
      const template: SESTemplate = {
        TemplateName: templateName,
        SubjectPart: 'subject',
        HtmlPart: '<p>some text</p>',
        TextPart: 'some text'
      }
      beforeEach(() => {
        fakeSES._templates.set(templateName, template)
        fakeSES._metadata.set(templateName, {Name: templateName, CreatedTimestamp: new Date('12/12/1997 01:01:01')})
      })

      it('should return list of one', async () => {
        const result = await getTemplates(fakeSES as unknown as SES)({} as APIGatewayProxyEvent)
        expect(result.statusCode).toEqual(200)
        expect(fakeSES.templateName).toEqual('something')
        expect(JSON.parse(result.body)).toEqual([{
          title: 'something',
          subject: 'subject',
          htmlText: '<p>some text</p>',
          text: 'some text'
        }])
      })
    })
  })
})