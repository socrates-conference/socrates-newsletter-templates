import {
  CreateTemplateCommandInput,
  CreateTemplateCommandOutput,
  DeleteTemplateCommandInput,
  DeleteTemplateCommandOutput,
  GetTemplateCommandInput,
  GetTemplateCommandOutput,
  ListTemplatesCommandInput,
  ListTemplatesCommandOutput,
  TemplateMetadata,
  UpdateTemplateCommandInput,
  UpdateTemplateCommandOutput
} from '@aws-sdk/client-ses'

export type SESTemplate = {
  TemplateName: string,
  SubjectPart: string,
  HtmlPart: string,
  TextPart: string
}

export class FakeSES {
  public templateName: string
  public _templates: Map<string, SESTemplate>
  public _metadata: Map<string, TemplateMetadata>

  constructor() {
    this._metadata = new Map()
    this._templates = new Map()
  }
  // noinspection JSUnusedLocalSymbols
  public listTemplates = async (options: ListTemplatesCommandInput): Promise<ListTemplatesCommandOutput> => {
    this.templateName = undefined
    return {TemplatesMetadata: [...this._metadata.values()]} as ListTemplatesCommandOutput
  }

  public getTemplate = async (options: GetTemplateCommandInput): Promise<GetTemplateCommandOutput> => {
    const {TemplateName} = options
    this.templateName = TemplateName
    const template: SESTemplate = this._templates.get(TemplateName)
    if(template === undefined) throw {__type: 'TemplateDoesNotExist'}
    return {Template: template} as GetTemplateCommandOutput
  }

  public createTemplate = async (options: CreateTemplateCommandInput): Promise<CreateTemplateCommandOutput> => {
    this.templateName = options.Template.TemplateName
    this._templates.set(this.templateName, options.Template as SESTemplate)
    this._metadata.set(this.templateName, {Name: this.templateName, CreatedTimestamp: new Date('12/17/1997 17:37:16')})

    return {} as CreateTemplateCommandOutput
  }

  public updateTemplate = async (options: UpdateTemplateCommandInput): Promise<UpdateTemplateCommandOutput> => {
    this.templateName = options.Template.TemplateName
    this._templates.set(this.templateName, options.Template as SESTemplate)
    this._metadata.set(this.templateName, {Name: this.templateName, CreatedTimestamp: new Date('12/17/1997 17:37:16')})

    return {} as CreateTemplateCommandOutput
  }

  public deleteTemplate = async (options: DeleteTemplateCommandInput): Promise<DeleteTemplateCommandOutput> => {
    this.templateName = options.TemplateName
    if (this._templates.has(this.templateName)) {
      this._templates.delete(this.templateName)
      this._metadata.delete(this.templateName)
      return {} as DeleteTemplateCommandOutput
    } else {
      throw  {
        __type: 'ResourceNotFoundException'
      }
    }
  }
}