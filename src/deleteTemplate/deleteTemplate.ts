import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  failure,
  success
} from '../common/util'
import {SES} from '@aws-sdk/client-ses'

export const deleteTemplate = (ses: SES) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const templateName = event?.pathParameters?.template
  if (templateName !== undefined) {
    try {
      await ses.deleteTemplate({TemplateName: templateName})
      return success()
    } catch (e) {
      return e.__type.endsWith('ResourceNotFoundException')
             ? failure(404, e.message)
             : failure(500, e.message)
    }
  } else {
    return failure(400, 'Invalid parameters.')
  }
}