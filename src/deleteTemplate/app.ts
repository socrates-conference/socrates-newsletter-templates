import {deleteTemplate} from './deleteTemplate'
import {SES} from '@aws-sdk/client-ses'

export const handler = deleteTemplate(new SES({region: 'eu-central-1'}))
