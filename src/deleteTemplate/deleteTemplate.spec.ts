import {deleteTemplate} from './deleteTemplate'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {
  FakeSES,
  SESTemplate
} from '../mock/ses'
import {SES} from '@aws-sdk/client-ses'


describe('DELETE /templates:', function () {
  const inputEvent: APIGatewayProxyEvent = {pathParameters: {template: 'something'}} as unknown as APIGatewayProxyEvent
  let fakeSES: FakeSES
  beforeEach(() => {
    fakeSES = new FakeSES()
  })

  describe('when a template exists', () => {
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      const templateName = 'something'
      const template: SESTemplate = {
        TemplateName: templateName,
        SubjectPart: 'subject',
        HtmlPart: '<p>some text</p>',
        TextPart: 'some text'
      }
      fakeSES._templates.set(templateName, template)
      fakeSES._metadata.set(templateName, {Name: templateName, CreatedTimestamp: new Date('12/12/1997 01:01:01')})
      result = await deleteTemplate(fakeSES as unknown as SES)(inputEvent)
    })
    it('should delete template named "something"', function () {
      expect(fakeSES.templateName).toEqual('something')
    })
    it('should return 200', async () => {
      expect(result.statusCode).toEqual(200)
    })
  })

  describe('when an item does not exist', () => {
    let result: APIGatewayProxyResult
    beforeEach(async () => {
      result = await deleteTemplate(fakeSES as unknown as SES)(inputEvent)
    })


    it('should return 404', () => {
      expect(result.statusCode).toEqual(404)
    })
  })


})