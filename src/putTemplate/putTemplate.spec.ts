import {FakeSES} from '../mock/ses'

import {APIGatewayProxyResult} from 'aws-lambda'
import {putTemplate} from './putTemplate'
import {SES} from '@aws-sdk/client-ses'


describe('PUT /templates', () => {
  let result: APIGatewayProxyResult
  const mock: FakeSES = new FakeSES()

  beforeAll(async () => {
    const event: any = {
      body: JSON.stringify({
        title: 'something',
        subject: 'my subject',
        htmlText: '<p>some text</p>',
        text: 'some text'
      })
    }
    result = await putTemplate(mock as unknown as SES)(event)
  })

  it('should return status 200', async () => {
    expect(result.statusCode).toEqual(200)
  })

  it('should store a valid template', async () => {
    expect(mock.templateName).toEqual('something')
    expect((await mock.listTemplates({})).TemplatesMetadata[0].Name).toEqual( 'something')
  })

  describe('when a template already exists', ()=>{
    beforeAll(async () => {
      const event: any = {
        body: JSON.stringify({
          title: 'something',
          subject: 'my updated subject',
          htmlText: '<p>some text</p>',
          text: 'some text'
        })
      }
      result = await putTemplate(mock as unknown as SES)(event)
    })

    it('should return status 200', async () => {
      expect(result.statusCode).toEqual(200)
    })

    it('should not add another template', async () => {
      expect((await mock.listTemplates({})).TemplatesMetadata.length).toEqual( 1)
    })

    it('should change the updated subject', async () => {
      expect((await mock.getTemplate({TemplateName:'something'})).Template.SubjectPart).toEqual( 'my updated subject')
    })
  })
})