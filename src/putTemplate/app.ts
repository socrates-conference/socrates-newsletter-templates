import {putTemplate} from './putTemplate'
import {SES} from '@aws-sdk/client-ses'

export const handler = putTemplate(new SES({region: 'eu-central-1'}))