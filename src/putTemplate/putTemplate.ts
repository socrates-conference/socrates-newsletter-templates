import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult
} from 'aws-lambda'
import {Template} from '../common/types'
import {
  failure,
  success
} from '../common/util'
import {
  CreateTemplateCommandInput,
  SES,
  UpdateTemplateCommandInput
} from '@aws-sdk/client-ses'

const toTemplateInput = (body: string): CreateTemplateCommandInput => {
  const payload: Template = JSON.parse(body)
  return {
    Template: {
      TemplateName: payload.title,
      HtmlPart: payload.htmlText,
      TextPart: payload.text,
      SubjectPart: payload.subject
    }
  }
}

export const putTemplate = (ses: SES) => async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const createTemplate = async (): Promise<APIGatewayProxyResult> => {
    const input: CreateTemplateCommandInput = toTemplateInput(event.body)
    await ses.createTemplate(input)
    return success()
  }

  const updateTemplate = async (): Promise<APIGatewayProxyResult> => {
    const input: UpdateTemplateCommandInput = toTemplateInput(event.body)
    await ses.updateTemplate(input)
    return success()
  }

  const templateExists = async (): Promise<boolean> => {
    try {
      await ses.getTemplate({TemplateName: JSON.parse(event.body).title})
      return true
    } catch (e) {
      return false
    }
  }

  try {
    return await templateExists()
      .then(result => result ? updateTemplate() : createTemplate())
  } catch (e) {
    return e.type === 'ValidationError'
           ? failure(400, 'The input arguments were invalid.')
           : failure(e.message)
  }
}